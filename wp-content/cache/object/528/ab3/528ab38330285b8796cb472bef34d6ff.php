��=Z<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:378;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2016-10-17 08:50:31";s:13:"post_date_gmt";s:19:"2016-10-17 08:50:31";s:12:"post_content";s:10652:"[tm_pb_section admin_label="section" transparent_background="on" allow_player_pause="off" inner_shadow="off" parallax="off" parallax_method="off" padding_mobile="off" make_fullwidth="off" use_custom_width="off" width_unit="on" make_equal="off" use_custom_gutter="off" background_color="#dd3333"][tm_pb_row admin_label="Row"][tm_pb_column type="4_4"][tm_pb_text admin_label="Text" text_orientation="left" use_border_color="off" border_color="#ffffff" border_style="solid"]

<h1>Rooms &amp; Suites</h1>

[/tm_pb_text][/tm_pb_column][/tm_pb_row][tm_pb_row admin_label="Row" make_fullwidth="off" use_custom_width="off" use_grid_padding="on" width_unit="on" custom_padding="||23px|" padding_mobile="off" allow_player_pause="off" parallax="off" parallax_method="off" make_equal="off" parallax_1="off" parallax_method_1="off" parallax_2="off" parallax_method_2="off" column_padding_mobile="on"][tm_pb_column type="1_2"][tm_pb_text admin_label="Text" text_orientation="left" use_border_color="off" border_color="#ffffff" border_style="solid"]

<p>
While our hotel has a standard set of the room types, there's absolutely nothing standard about them! Each single room, double or family room, each suite we offer is enhanced with a little extra of its own!</p>

[/tm_pb_text][/tm_pb_column][tm_pb_column type="1_2"][tm_pb_text admin_label="Text" text_orientation="left" use_border_color="off" border_color="#ffffff" border_style="solid"]

<p>
The intricate and elegant, but at the same time comfortable interior and exterior of the hotel make your stay here a blast! Additionally, all the amenities that we have, hotel-wide WiFI &amp; our incredible eat &amp; drink venues will be a cherry on the top!</p>

[/tm_pb_text][/tm_pb_column][/tm_pb_row][/tm_pb_section][tm_pb_section admin_label="Section" fullwidth="off" specialty="off" transparent_background="on" allow_player_pause="off" inner_shadow="off" parallax="off" parallax_method="off" custom_padding="||50px|" custom_padding_tablet="||20px|" custom_padding_phone="||20px|" custom_padding_last_edited="on|phone" padding_mobile="off" make_fullwidth="off" use_custom_width="off" width_unit="on" make_equal="off" use_custom_gutter="off"][tm_pb_row admin_label="Row" make_fullwidth="off" use_custom_width="off" use_grid_padding="on" width_unit="on" padding_mobile="off" allow_player_pause="off" parallax="off" parallax_method="off" make_equal="off" parallax_1="off" parallax_method_1="off" parallax_2="off" parallax_method_2="off" parallax_3="off" parallax_method_3="off" column_padding_mobile="on" module_id="room-list"][tm_pb_column type="1_3"][tm_pb_image admin_label="Image" src="http://thenobleswanhotel.com/wp-content/uploads/2017/12/the-deluxe-window-rooom-1.jpg" title_text="THE DELUXE WINDOW ROOM" show_in_lightbox="off" url_new_window="off" use_overlay="off" animation="left" sticky="off" align="center" force_fullwidth="off" always_center_on_mobile="on" use_border_color="off" border_color="#ffffff" border_style="solid" url="/rooms-suites/the-deluxe-window-room/"]



[/tm_pb_image][tm_pb_text admin_label="Text" text_orientation="left" use_border_color="off" border_color="#ffffff" border_style="solid"]

<h3><a href="/rooms-suites/the-deluxe-window-room/">THE DELUXE WINDOW ROOM</a></h3>
<div class="mphb_sc_rooms-wrapper">
<ul class="mphb-loop-room-type-attributes">
<li class="mphb-room-type-view">View: <strong>City</strong></li>
<li class="mphb-room-type-size">Size: <strong>23m²</strong></li>
<li class="mphb-room-type-adults-capacity">Adults: <b>2</b></li>
</ul>
<p class="mphb-regular-price"><strong>Price Per Night:</strong><span class="mphb-price"><span class="mphb-currency">$</span>250</span><span class="mrhb-price-suffix">/per night</span></p>
</div>

[/tm_pb_text][tm_pb_button admin_label="Button" button_url="/room-availability/" url_new_window="off" button_text="BOOK NOW" button_alignment="left" custom_button="off" button_letter_spacing="0" button_use_icon="default" button_icon_placement="right" button_on_hover="on" button_letter_spacing_hover="0"]



[/tm_pb_button][/tm_pb_column][tm_pb_column type="1_3"][tm_pb_image admin_label="Image" src="http://thenobleswanhotel.com/wp-content/uploads/2017/12/executive-windows-room-1.jpg" title_text="THE DELUXE WINDOW ROOM" show_in_lightbox="off" url_new_window="off" use_overlay="off" animation="left" sticky="off" align="center" force_fullwidth="off" always_center_on_mobile="on" use_border_color="off" border_color="#ffffff" border_style="solid" url="/rooms-suites/executive-window-room/"]



[/tm_pb_image][tm_pb_text admin_label="Text" text_orientation="left" use_border_color="off" border_color="#ffffff" border_style="solid"]

<h3><a href="/rooms-suites/executive-window-room/">EXECUTIVE WINDOW ROOM</a></h3>
<div class="mphb_sc_rooms-wrapper">
<ul class="mphb-loop-room-type-attributes">
<li class="mphb-room-type-view">View: <strong>City</strong></li>
<li class="mphb-room-type-size">Size: <strong>28m²</strong></li>
<li class="mphb-room-type-adults-capacity">Adults: <b>2</b></li>
</ul>
<p class="mphb-regular-price"><strong>Price Per Night:</strong><span class="mphb-price"><span class="mphb-currency">$</span>250</span><span class="mrhb-price-suffix">/per night</span></p>
</div>

[/tm_pb_text][tm_pb_button admin_label="Button" button_url="/room-availability/" url_new_window="off" button_text="BOOK NOW" button_alignment="left" custom_button="off" button_letter_spacing="0" button_use_icon="default" button_icon_placement="right" button_on_hover="on" button_letter_spacing_hover="0"]



[/tm_pb_button][/tm_pb_column][tm_pb_column type="1_3"][tm_pb_image admin_label="Image" src="http://thenobleswanhotel.com/wp-content/uploads/2017/12/phong-1-1-2.jpg" title_text="THE DELUXE WINDOW ROOM" show_in_lightbox="off" url_new_window="off" use_overlay="off" animation="left" sticky="off" align="center" force_fullwidth="off" always_center_on_mobile="on" use_border_color="off" border_color="#ffffff" border_style="solid" url="/rooms-suites/executive-balcony-room/"]



[/tm_pb_image][tm_pb_text admin_label="Text" text_orientation="left" use_border_color="off" border_color="#ffffff" border_style="solid"]

<h3><a href="/rooms-suites/executive-balcony-room/">EXECUTIVE BALCONY ROOM</a></h3>
<div class="mphb_sc_rooms-wrapper">
<ul class="mphb-loop-room-type-attributes">
<li class="mphb-room-type-view">View: <strong>City</strong></li>
<li class="mphb-room-type-size">Size: <strong>28m²</strong></li>
<li class="mphb-room-type-adults-capacity">Adults: <b>2</b></li>
</ul>
<p class="mphb-regular-price"><strong>Price Per Night:</strong><span class="mphb-price"><span class="mphb-currency">$</span>250</span><span class="mrhb-price-suffix">/per night</span></p>
</div>

[/tm_pb_text][tm_pb_button admin_label="Button" button_url="/room-availability/" url_new_window="off" button_text="BOOK NOW" button_alignment="left" custom_button="off" button_letter_spacing="0" button_use_icon="default" button_icon_placement="right" button_on_hover="on" button_letter_spacing_hover="0"]



[/tm_pb_button][/tm_pb_column][/tm_pb_row][tm_pb_row admin_label="Row" make_fullwidth="off" use_custom_width="off" use_grid_padding="on" width_unit="on" padding_mobile="off" allow_player_pause="off" parallax="off" parallax_method="off" make_equal="off" parallax_1="off" parallax_method_1="off" parallax_2="off" parallax_method_2="off" parallax_3="off" parallax_method_3="off" column_padding_mobile="on" module_id="room-list" custom_margin="50px|||" custom_margin_tablet="20px|||" custom_margin_phone="20px|||" custom_margin_last_edited="on|phone"][tm_pb_column type="1_3"][tm_pb_image admin_label="Image" src="http://thenobleswanhotel.com/wp-content/uploads/2017/12/phong-1-1-2.jpg" title_text="THE DELUXE WINDOW ROOM" show_in_lightbox="off" url_new_window="off" use_overlay="off" animation="left" sticky="off" align="center" force_fullwidth="off" always_center_on_mobile="on" use_border_color="off" border_color="#ffffff" border_style="solid" url="/rooms-suites/family-room/"]



[/tm_pb_image][tm_pb_text admin_label="Text" text_orientation="left" use_border_color="off" border_color="#ffffff" border_style="solid"]

<h3><a href="/rooms-suites/family-room/">FAMILY ROOM</a></h3>
<div class="mphb_sc_rooms-wrapper">
<ul class="mphb-loop-room-type-attributes">
<li class="mphb-room-type-view">View: <strong>City</strong></li>
<li class="mphb-room-type-size">Size: <strong>50m²</strong></li>
<li class="mphb-room-type-adults-capacity">Adults: <b>5</b></li>
</ul>
<p class="mphb-regular-price"><strong>Price Per Night:</strong><span class="mphb-price"><span class="mphb-currency">$</span>250</span><span class="mrhb-price-suffix">/per night</span></p>
</div>

[/tm_pb_text][tm_pb_button admin_label="Button" button_url="/room-availability/" url_new_window="off" button_text="BOOK NOW" button_alignment="left" custom_button="off" button_letter_spacing="0" button_use_icon="default" button_icon_placement="right" button_on_hover="on" button_letter_spacing_hover="0"]



[/tm_pb_button][/tm_pb_column][tm_pb_column type="1_3"][tm_pb_image admin_label="Image" src="http://thenobleswanhotel.com/wp-content/uploads/2017/12/connecting-room-1.jpg" title_text="THE DELUXE WINDOW ROOM" show_in_lightbox="off" url_new_window="off" use_overlay="off" animation="left" sticky="off" align="center" force_fullwidth="off" always_center_on_mobile="on" use_border_color="off" border_color="#ffffff" border_style="solid" url="/rooms-suites/connecting-room/"]



[/tm_pb_image][tm_pb_text admin_label="Text" text_orientation="left" use_border_color="off" border_color="#ffffff" border_style="solid"]

<h3><a href="/rooms-suites/connecting-room/">CONNECTING ROOM</a></h3>
<div class="mphb_sc_rooms-wrapper">
<ul class="mphb-loop-room-type-attributes">
<li class="mphb-room-type-view">View: <strong>City</strong></li>
<li class="mphb-room-type-size">Size: <strong>60m²</strong></li>
<li class="mphb-room-type-adults-capacity">Adults: <b>5</b></li>
</ul>
<p class="mphb-regular-price"><strong>Price Per Night:</strong><span class="mphb-price"><span class="mphb-currency">$</span>250</span><span class="mrhb-price-suffix">/per night</span></p>
</div>

[/tm_pb_text][tm_pb_button admin_label="Button" button_url="/room-availability/" url_new_window="off" button_text="BOOK NOW" button_alignment="left" custom_button="off" button_letter_spacing="0" button_use_icon="default" button_icon_placement="right" button_on_hover="on" button_letter_spacing_hover="0"]



[/tm_pb_button][/tm_pb_column][tm_pb_column type="1_3"][/tm_pb_column][/tm_pb_row][/tm_pb_section]";s:10:"post_title";s:14:"Rooms & Suites";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:12:"rooms-suites";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2017-12-19 10:39:50";s:17:"post_modified_gmt";s:19:"2017-12-19 03:39:50";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:71:"http://192.168.9.80/2016/6_June/13.Roberta.58970.Woods.WP.update/rooms/";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}