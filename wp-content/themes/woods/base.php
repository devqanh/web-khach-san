<?php get_header( woods_template_base() ); ?>

	<?php do_action( 'woods_render_widget_area', 'full-width-header-area' ); ?>

	<div <?php echo woods_get_container_classes( array( 'site-content_wrap' ), 'content' ); ?>>

		<?php do_action( 'woods_render_widget_area', 'before-content-area' ); ?>

		<div class="row">

			<div id="primary"  <?php cuongdc_check_if_in_tour()?'class="col-xs-12 col-md-12"':  woods_primary_content_class(); ?>>

				<?php do_action( 'woods_render_widget_area', 'before-loop-area' ); ?>

				<main id="main" class="site-main" role="main">

					<?php include woods_template_path(); ?>

				</main><!-- #main -->

				<?php do_action( 'woods_render_widget_area', 'after-loop-area' ); ?>

			</div><!-- #primary -->

			<?php get_sidebar(); // Loads the sidebar.php template.  ?>

		</div><!-- .row -->

		<?php do_action( 'woods_render_widget_area', 'after-content-area' ); ?>

	</div><!-- .container -->

	<?php do_action( 'woods_render_widget_area', 'after-content-full-width-area' ); ?>

<?php get_footer( woods_template_base() ); ?>
